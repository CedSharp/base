const webpack = require('webpack')
const webpackConfig = require('../webpack.common')

const watchJs = cb => {
  const jsCompiler = webpack(webpackConfig)
  jsCompiler.watch({}, (err, stats) => {
    if(err) {
      console.log('\n!!! JS ERROR !!!\n')
      console.log(stats.toJson('minimal'))
      console.log()
    }
    else 
      console.log('['+(new Date).toLocaleTimeString()+']: JS Updated')
  })
}

module.exports = watchJs