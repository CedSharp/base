const path = require('path')
const { series, src, dest, watch } = require('gulp')
const rename = require('gulp-rename')
const postcss = require('gulp-postcss')
const sass = require('gulp-sass')
const autoprefixer = require('autoprefixer')
const mqpacker = require('css-mqpacker')
const cssnano = require('cssnano')
const config = require('../config')

const SASS_FILES = path.join(config.sass.src.dir, '**', '*.{sass,scss}')
const preprocessors = [
  autoprefixer({ flexbox: 'no-2009', grid: 'autoplace' }),
  mqpacker(),
  cssnano
]

const compileSass = done => {
  console.log('['+(new Date).toLocaleTimeString()+']: Sass Updated')
  return src(path.join(config.sass.src.dir, config.sass.src.file), { sourcemaps: true })
    .pipe(sass().on('error', err => {
      console.log('\n!!! Sass Error !!!\n')
      console.log(err)
      console.log()
    }))
    .pipe(postcss(preprocessors))
    .pipe(rename(config.sass.dest.file))
    .pipe(dest(config.sass.dest.dir))
    .on('end', done)
}

const watchSass = series(compileSass, () => watch(SASS_FILES, compileSass))

module.exports = watchSass