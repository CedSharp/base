const path = require('path')
const merge = require('webpack-merge')
const pnp = require('pnp-webpack-plugin')
const config = require('./config')
const prod = require('./webpack.production')
const dev = require('./webpack.development')

const env = process.env.NODE_ENV || 'development'
const additionalConfig = env === 'production' ? prod : dev

const commonConfig = {
  entry: [
    'regenerator-runtime', 
    path.join(config.js.src.dir, config.js.src.file)
  ],
  output: {
    path: config.js.dest.dir,
    filename: config.js.dest.file,
    libraryTarget: 'commonjs2'
  },
  mode: env,
  target: 'web',
  stats: 'minimal',
  resolve: {
    // -- Allow using Yarn2, which doesn't use node_modules/ --
    plugins: [ pnp ],
    extensions: ['.js', '.json'],
    alias: {
      '@': config.source
    }
  },
  resolveLoader: {
    // -- Allow using Yarn2, which doesn't use node_modules/ --
    plugins: [ pnp.moduleLoader(module) ]
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: [ /node_modules/ ],
        loader: require.resolve('babel-loader'),
        options: {
          presets: [ '@babel/preset-env' ],
          plugins: [ '@babel/plugin-transform-runtime' ]
        }
      }
    ]
  },
  plugins: [

  ]
}

module.exports = merge(commonConfig, additionalConfig)