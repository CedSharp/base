const requireDir = require('require-dir')
const { parallel } = require('gulp')

const tasks = requireDir('./tasks')

const watchAll = done => {
  console.log('Base Sass/ES6 watcher...\nUse CTRL+C or CMD+C to stop.\n')
  return parallel(...Object.values(tasks))(done)
}

module.exports = { default: watchAll, ...tasks }