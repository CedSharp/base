const path = require('path')

const ROOT = path.resolve(__dirname, '..')
const SOURCE = path.resolve(ROOT, 'src')
const DESTINATION = path.resolve(ROOT, 'dist')

module.exports = {
  source: SOURCE,
  destination: DESTINATION,
  js: {
    src: {
      dir: path.resolve(SOURCE, 'js'),
      file: 'main.js'
    },
    dest: {
      dir: path.resolve(DESTINATION, 'assets', 'js'),
      file: 'app.js'
    }
  },
  sass: {
    src: {
      dir: path.resolve(SOURCE, 'sass'),
      file: 'styles.scss'
    },
    dest: {
      dir: path.resolve(DESTINATION, 'assets', 'css'),
      file: 'styles.css'
    }
  }
}