# Base

A simple minimalistic, build-dependencies hidden setup for Sass+ES6.
Also has a small django manage.py-style "do" script to do all actions.

### Building

`./do build`

### Development

`./do dev`

### Linting

`./do lint`