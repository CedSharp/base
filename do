#! /usr/bin/env bash
case "$1" in
	build)
		echo "Production builds are not yet implemented"
		;;
	dev)
		cd .vendor && clear && yarn start
		;;
	install)
		cd .vendor && clear && yarn install && clear && echo "All dependencies installed"
		;;
	lint)
		cd .vendor && clear && yarn lint
		;;
esac