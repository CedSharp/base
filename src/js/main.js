const wait = async time => new Promise(resolve => setTimeout(resolve, time))

const app = async () => {
  await wait(1000)
  console.log('Hello ES6')
}

app()